from src.services.executor import ChatEvaluator
from src.mocks.ground_truth import references
import os



def evaluate(flow):
    obj = ChatEvaluator("ATMR_itau_PT")
    module = os.getenv("MODULE")
    # flow = os.getenv("FLOW")
    
    text_responses_flow = obj.evaluate_conversation(module, flow)
    response_references = references[module][flow]
    rouge_score_result = obj.compute_metrics(text_responses_flow, response_references)
    print(rouge_score_result)
    

if __name__ == "__main__":
    evaluate(flow='flow2')