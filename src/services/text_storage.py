class TextHandler:
    @classmethod
    def get_text(cls, chat_response):
        return [text.text for text in chat_response.response]

    # @classmethod
    # def get_module(cls, chat_)
