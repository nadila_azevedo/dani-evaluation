import asyncio
from dani_context.adapters.loaders.vector_loader_local import VectorLoaderLocal
from dani_context.chatbot_agent.chat_executor import DaniChatExecutor
from dani_context.chatbot_agent.models.chat_model import ChatConfig
from dani_context.chatbot_agent.prompts.prompt import Prompt
from dani_context.utils.mocks.pt.itau.aggregator import aggregator
from src.services.text_storage import TextHandler
from src.mocks.flows import flows
from src.mocks.utils.metrics import Metrics

loop = asyncio.get_event_loop()


class ChatEvaluator:
    def __init__(self, embedding) -> None:
        self.embedding = embedding
        self.chat_config = self._create_chat_config()

    def _create_chat_config(self):
        """Create and return a ChatConfig object."""
        return ChatConfig(conversations_index=self.embedding, score_th_maintenance=0.1)

    async def _load_vector(self):
        """Asynchronously load the vector using VectorLoaderLocal."""
        return await VectorLoaderLocal.load(self.chat_config)

    def load_vector_sync(self):
        """Synchronously load the vector using asyncio loop."""
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(self._load_vector())
    
    def execute_chat(self, interaction, history, current_module):
        docsearch_conv = self.load_vector_sync()
        chat = DaniChatExecutor(
            self.chat_config,
            docsearch_conv,
            Prompt.get_conversational_prompt_pt(),
            history,
        )
        
        chatbot = loop.run_until_complete(chat.main(interaction, current_module, "pt", aggregator))
        module = chatbot.module
        next_history = chat.dani_maintenance_chat.maintenance_chat_history
        return chatbot, next_history, module
    
    def evaluate_conversation(self, module_under_testing, flow):
        history = []
        conversation = flows.get(module_under_testing)
        interactions = conversation.get(flow)
        current_module = None
        interaction_responses = []
        for interaction in interactions:
            response, next_history, module = self.execute_chat(interaction, history.copy(), current_module)
            interaction_responses.append(response)
            history = next_history
            current_module = module

        
        text_responses = []
        for resp in interaction_responses:
            text_resp = TextHandler.get_text(resp)

            if isinstance(text_resp, list):
                text_resp = ' '.join(text_resp)  # You can customize the separator if needed
            text_responses.append(text_resp)

        return text_responses


    # def execute_chat(self, interaction, history, current_module):
    #     """Execute a single chat interaction."""
    #     docsearch_conv = self.load_vector_sync()
    #     chat_executor = DaniChatExecutor(
    #             self.chat_config,
    #             docsearch_conv,
    #             Prompt.get_conversational_prompt_pt(),
    #             history,
    #         )
    
    #     chatbot_response = loop.run_until_complete(chat_executor.main(
    #         interaction, current_module, "pt", aggregator
    #     ))
    #     next_history = chat_executor.dani_maintenance_chat.maintenance_chat_history

    #     return chatbot_response, next_history, chatbot_response.module



    @staticmethod
    def compute_metrics(predictions, references):
        """Compute evaluation metrics for the given predictions and references."""
        return Metrics().get_metrics_score(predictions, references)
