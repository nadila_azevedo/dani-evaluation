import evaluate
import Levenshtein
import numpy as np
class Metrics:
    def get_rouge_score(self, predictions, references):
        rouge = evaluate.load("rouge")
        print(predictions)
        print(references)
        results = rouge.compute(predictions=predictions, references=references)
        return results

    def get_bleur_score(self, predictions, references):
        bleu = evaluate.load("bleu")
        bleu_results = bleu.compute(
            predictions=predictions, references=[[ref] for ref in references]
        )
        return bleu_results

    def get_meteor_score(self, predictions, references):
        meteor = evaluate.load("meteor")
        meteor_results = meteor.compute(predictions=predictions, references=references)
        return meteor_results
    
    def get_levenshtein_score(self, predictions, references):
        ratios = []
        for prediction, reference in zip(predictions, references):
            ratios.append(Levenshtein.ratio(prediction, 
                                            reference))
        
        ratios_array = np.array(ratios)
        mean_score = np.mean(ratios_array)
        return mean_score

    def get_metrics_score(self, predictions, references):
        rouge = self.get_rouge_score(predictions, references)
        bleu = self.get_bleur_score(predictions, references)
        meteor = self.get_meteor_score(predictions, references)
        levenshtein_mean_score = self.get_levenshtein_score(predictions, references)
        return {"rouge": rouge, "bleu": bleu, "meteo": meteor, "levenstein_mean_score": levenshtein_mean_score}
    
    
            
