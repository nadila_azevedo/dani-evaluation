references = {
    "Reciclador": {
        "flow1": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador."
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP)."
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Passo 2: Verifique se há numerário enroscado no bocal de entrada na Fechadura 2. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo. Confira o processo de desenrosco no bocal de entrada no vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 3: Verifique se há numerário enroscado na unidade superior. Caso positivo, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Assista o procedimento abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 4: Também será preciso verificar possível enrosco na parte inferior da unidade superior."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 5:  Verifique se há numerário enroscado na Fechadura 1. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 6: Retire os cassetes para verificar se há enrosco nos rolos no final do módulo ou cédulas presas dentro do cassete. Após verificação, recoloque os cassetes nas respectivas posições de onde foram retirados e feche as portas do equipamento."
            "Conseguimos solucionar o seu problema?",
            
            "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂",
        ],
        "flow2": [
            " Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador \m{#T18}.\n"
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP).\n"
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂 ",
        ],
        "flow3": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador."
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP)."
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Passo 2: Verifique se há numerário enroscado no bocal de entrada na Fechadura 2. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo. Confira o processo de desenrosco no bocal de entrada no vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂 ",
        ],
        "flow4": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador."
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP)."
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Passo 2: Verifique se há numerário enroscado no bocal de entrada na Fechadura 2. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo. Confira o processo de desenrosco no bocal de entrada no vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 3: Verifique se há numerário enroscado na unidade superior. Caso positivo, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Assista o procedimento abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂 ",
        ],
        "flow5": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador."
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP)."
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Passo 2: Verifique se há numerário enroscado no bocal de entrada na Fechadura 2. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo. Confira o processo de desenrosco no bocal de entrada no vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 3: Verifique se há numerário enroscado na unidade superior. Caso positivo, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Assista o procedimento abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 4: Também será preciso verificar possível enrosco na parte inferior da unidade superior."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂 ",
            ],
        "flow6": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador."
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP)."
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Passo 2: Verifique se há numerário enroscado no bocal de entrada na Fechadura 2. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo. Confira o processo de desenrosco no bocal de entrada no vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 3: Verifique se há numerário enroscado na unidade superior. Caso positivo, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Assista o procedimento abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 4: Também será preciso verificar possível enrosco na parte inferior da unidade superior."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 5:  Verifique se há numerário enroscado na Fechadura 1. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂 ",
            ],
        "flow7": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Identifiquei que o seu problema é relacionado ao reciclador."
            "Passo 1: Abra a Fechadura 2 e a Fechadura 1. Verifique se o ATMR possui sistema Security Way ativado. Caso possua, faça a liberação da Fechadura 1 via Painel do Operador (IOP)."
            "Veja um exemplo da tela do IOP com o Security Way ativado a seguir 👇. Conseguiu realizar o procedimento?",
            
            "Passo 2: Verifique se há numerário enroscado no bocal de entrada na Fechadura 2. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo. Confira o processo de desenrosco no bocal de entrada no vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 3: Verifique se há numerário enroscado na unidade superior. Caso positivo, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Assista o procedimento abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 4: Também será preciso verificar possível enrosco na parte inferior da unidade superior."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 5:  Verifique se há numerário enroscado na Fechadura 1. Caso exista, desenrosque a cédula com cuidado para não danificar nenhum mecanismo do módulo."
            "Realize o procedimento conforme o vídeo abaixo 👇"
            "Conseguiu realizar o procedimento?",
            
            "Passo 6: Retire os cassetes para verificar se há enrosco nos rolos no final do módulo ou cédulas presas dentro do cassete. Após verificação, recoloque os cassetes nas respectivas posições de onde foram retirados e feche as portas do equipamento."
            "Conseguimos solucionar o seu problema?",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂 ",
            ],
    },
    "Impressora de recibos": {
        "flow1": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado à impressora de recibos 📠🛠️ .\n"
            "Passo 1: verifique o modelo de impressora no seu ATM por meio da etiqueta presente na porta traseira do gabinete superior."
            "\*a) Formato ATMR-4534-(428, 450 e 460) - modelo Itatiaia."
            "\*b) Formato ATMR-4534-(495, 496, 497, 522 e 523) - modelo TP31."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nConseguiu realizar o procedimento?\n"
            "Continue até o final.\n",
            
            "Passo 2: Após identificar a sua impressora, abra a porta do gabinete superior (porta da impressora). Verifique se há enrosco de papel no bocal da impressora. "
            "Caso exista, retire a bobina de papel, em seguida, verifique e remova o resíduo. "
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 3: recoloque a bobina de papel, conforme o vídeo abaixo considerando o seu modelo de impressora."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nRealize as seguintes verificações e procedimentos."
            "\*a) Verifique se não há interferências no giro da bobina de papel. "
            "\*b) Verifique se a posição do papel na impressora não está fisicamente inserida ao contrário, ou seja, com a parte térmica invertida. "
            "\*c) Caso a bobina esteja fisicamente próxima do fim, efetue a troca "
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 4: posicione a impressora e certifique-se de que a trava (verde ou azul) está encaixada e feche a porta do gabinete superior. "
            "\nVocê conseguiu realizar todos os procedimentos?\n",
            
            "Fico feliz em saber! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂",
            ], 
        "flow2": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado à impressora de recibos 📠🛠️ .\n"
            "Passo 1: verifique o modelo de impressora no seu ATM por meio da etiqueta presente na porta traseira do gabinete superior."
            "\*a) Formato ATMR-4534-(428, 450 e 460) - modelo Itatiaia."
            "\*b) Formato ATMR-4534-(495, 496, 497, 522 e 523) - modelo TP31."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nConseguiu realizar o procedimento?\n"
            "Continue até o final.\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ],
        "flow3": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado à impressora de recibos 📠🛠️ .\n"
            "Passo 1: verifique o modelo de impressora no seu ATM por meio da etiqueta presente na porta traseira do gabinete superior."
            "\*a) Formato ATMR-4534-(428, 450 e 460) - modelo Itatiaia."
            "\*b) Formato ATMR-4534-(495, 496, 497, 522 e 523) - modelo TP31."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nConseguiu realizar o procedimento?\n"
            "Continue até o final.\n",
            
            "Passo 2: Após identificar a sua impressora, abra a porta do gabinete superior (porta da impressora). Verifique se há enrosco de papel no bocal da impressora. "
            "Caso exista, retire a bobina de papel, em seguida, verifique e remova o resíduo. "
            "\nConseguiu realizar o procedimento?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ],
        "flow4": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado à impressora de recibos 📠🛠️ .\n"
            "Passo 1: verifique o modelo de impressora no seu ATM por meio da etiqueta presente na porta traseira do gabinete superior."
            "\*a) Formato ATMR-4534-(428, 450 e 460) - modelo Itatiaia."
            "\*b) Formato ATMR-4534-(495, 496, 497, 522 e 523) - modelo TP31."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nConseguiu realizar o procedimento?\n"
            "Continue até o final.\n",
            
            "Passo 2: Após identificar a sua impressora, abra a porta do gabinete superior (porta da impressora). Verifique se há enrosco de papel no bocal da impressora. "
            "Caso exista, retire a bobina de papel, em seguida, verifique e remova o resíduo. "
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 3: recoloque a bobina de papel, conforme o vídeo abaixo considerando o seu modelo de impressora."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nRealize as seguintes verificações e procedimentos."
            "\*a) Verifique se não há interferências no giro da bobina de papel. "
            "\*b) Verifique se a posição do papel na impressora não está fisicamente inserida ao contrário, ou seja, com a parte térmica invertida. "
            "\*c) Caso a bobina esteja fisicamente próxima do fim, efetue a troca "
            "\nConseguiu realizar o procedimento?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ],
        "flow5": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado à impressora de recibos 📠🛠️ .\n"
            "Passo 1: verifique o modelo de impressora no seu ATM por meio da etiqueta presente na porta traseira do gabinete superior."
            "\*a) Formato ATMR-4534-(428, 450 e 460) - modelo Itatiaia."
            "\*b) Formato ATMR-4534-(495, 496, 497, 522 e 523) - modelo TP31."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nConseguiu realizar o procedimento?\n"
            "Continue até o final.\n",
            
            "Passo 2: Após identificar a sua impressora, abra a porta do gabinete superior (porta da impressora). Verifique se há enrosco de papel no bocal da impressora. "
            "Caso exista, retire a bobina de papel, em seguida, verifique e remova o resíduo. "
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 3: recoloque a bobina de papel, conforme o vídeo abaixo considerando o seu modelo de impressora."
            "\nImpressora TP31 👇"
            "\nImpressora Itatiaia 👇"
            "\nRealize as seguintes verificações e procedimentos."
            "\*a) Verifique se não há interferências no giro da bobina de papel. "
            "\*b) Verifique se a posição do papel na impressora não está fisicamente inserida ao contrário, ou seja, com a parte térmica invertida. "
            "\*c) Caso a bobina esteja fisicamente próxima do fim, efetue a troca "
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 4: posicione a impressora e certifique-se de que a trava (verde ou azul) está encaixada e feche a porta do gabinete superior. "
            "\nVocê conseguiu realizar todos os procedimentos?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ],
        },
    "Monitor de vídeo": {
        "flow1": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Vamos tentar resolver o seu problema no monitor 🖥️🔧.\n"
            "Passo 1: Cheque se a máquina está conectada na tomada. Caso esteja desconectada reconecte o cabo.\n"
            "O ATM voltou a funcionar?\n",
            
            "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂",
            ], 
        "flow2": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Vamos tentar resolver o seu problema no monitor 🖥️🔧.\n"
            "Passo 1: Cheque se a máquina está conectada na tomada. Caso esteja desconectada reconecte o cabo.\n"
            "O ATM voltou a funcionar?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂",
            ],
        "flow3": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Vamos tentar resolver o seu problema no monitor 🖥️🔧.\n"
            "Passo 1: Cheque se a máquina está conectada na tomada. Caso esteja desconectada reconecte o cabo.\n"
            "O ATM voltou a funcionar?\n",
            
            "Passo 2: desligue totalmente o ATM, por meio do botão físico. Após 10 segundos, ligue novamente.\n"
            "O ATM está operante?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂",
            ],
        "flow4": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Vamos tentar resolver o seu problema no monitor 🖥️🔧.\n"
            "Passo 1: Cheque se a máquina está conectada na tomada. Caso esteja desconectada reconecte o cabo.\n"
            "O ATM voltou a funcionar?\n",
            
            "Passo 2: desligue totalmente o ATM, por meio do botão físico. Após 10 segundos, ligue novamente.\n"
            "O ATM está operante?\n",
            
            "Passo 3: Há alguma obstrução na leitora de cartão (ex.: clipe, papel, etc.)? Caso sim, tente retirar a obstrução e, após removê-la, aguarde 2 minutos.\n"
            "O ATM voltou a funcionar?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂",
            ],
        "flow5": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Vamos tentar resolver o seu problema no monitor 🖥️🔧.\n"
            "Passo 1: Cheque se a máquina está conectada na tomada. Caso esteja desconectada reconecte o cabo.\n"
            "O ATM voltou a funcionar?\n",
            
            "Passo 2: desligue totalmente o ATM, por meio do botão físico. Após 10 segundos, ligue novamente.\n"
            "O ATM está operante?\n",
            
            "Passo 3: Há alguma obstrução na leitora de cartão (ex.: clipe, papel, etc.)? Caso sim, tente retirar a obstrução e, após removê-la, aguarde 2 minutos.\n"
            "O ATM voltou a funcionar?\n",
            
            "IA: Passo 4: Abra a porta do gabinete superior (porta da impressora) e veja a cor do LED do monitor. "
            "\*a) LED verde, monitor apagado - chame assistência técnica;"
            "\*b) LED verde e aceso, laranja ou vermelho - desligue e ligue o monitor;"
            "\*c) LED apagado - pressione o botão para ligá-lo.\n"
            "Após b) ou c), se o LED estiver verde, feche o gabinete e confira se o monitor funciona.\n"
            "Conseguimos resolver o seu problema?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂",
            ],
        },
    "Scanner de cheques": {
        "flow1": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado ao scanner de cheques 💳🔧.\n"
            "Passo 1: abra a porta do gabinete superior (porta da impressora). Verifique se o cartucho inkjet no módulo está posicionado corretamente. Caso esteja, siga os procedimentos exibidos no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",

            "Continue até o final.\n"
            "Passo 2: neste passo vamos verificar se há enrosco no compartimento superior central ou compartimento superior traseiro ou na entrada do bocal scanner ou na entrada do cassete de cheques. Caso exista algum enrosco, efetue o desenrosco com cuidado para não danificar o módulo. Você pode verificar como realizar o procedimento no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 3: após o desenrosco, posicione e verifique se todos os itens estão devidamente travados e reposicione o módulo do scanner de cheques. Feche a porta do gabinete superior (porta da impressora). "
            "\nVocê conseguiu realizar todos os procedimentos?\n",
            
            "Fico feliz em saber! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂",
            ],
        "flow2": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado ao scanner de cheques 💳🔧.\n"
            "Passo 1: abra a porta do gabinete superior (porta da impressora). Verifique se o cartucho inkjet no módulo está posicionado corretamente. Caso esteja, siga os procedimentos exibidos no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ], 
        "flow3": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado ao scanner de cheques 💳🔧.\n"
            "Passo 1: abra a porta do gabinete superior (porta da impressora). Verifique se o cartucho inkjet no módulo está posicionado corretamente. Caso esteja, siga os procedimentos exibidos no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",
            
            "Continue até o final.\n"
            "Passo 2: neste passo vamos verificar se há enrosco no compartimento superior central ou compartimento superior traseiro ou na entrada do bocal scanner ou na entrada do cassete de cheques. Caso exista algum enrosco, efetue o desenrosco com cuidado para não danificar o módulo. Você pode verificar como realizar o procedimento no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ], 
        "flow4": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa."
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            
            "Eu identifiquei que o seu problema é relacionado ao scanner de cheques 💳🔧.\n"
            "Passo 1: abra a porta do gabinete superior (porta da impressora). Verifique se o cartucho inkjet no módulo está posicionado corretamente. Caso esteja, siga os procedimentos exibidos no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",
            
            "Continue até o final.\n"
            "Passo 2: neste passo vamos verificar se há enrosco no compartimento superior central ou compartimento superior traseiro ou na entrada do bocal scanner ou na entrada do cassete de cheques. Caso exista algum enrosco, efetue o desenrosco com cuidado para não danificar o módulo. Você pode verificar como realizar o procedimento no vídeo abaixo 👇"
            "\nConseguiu realizar o procedimento?\n",
            
            "Passo 3: após o desenrosco, posicione e verifique se todos os itens estão devidamente travados e reposicione o módulo do scanner de cheques. Feche a porta do gabinete superior (porta da impressora). "
            "\nVocê conseguiu realizar todos os procedimentos?\n",
            
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️.\n"
            "Caso não saiba transferir assista o vídeo abaixo 👇"
            "\nConversa encerrada. Até a próxima! 👋🙂.",
            ], 
},
    "Teclado do cliente": {"flow1": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Vamos tentar resolver o seu problema no teclado"
            "Passo 1: Verifique se há no teclado do cliente algum sinal de vandalismo, como objeto (ex: clipe, papel, etc.) travando alguma tecla.\n"
            "Se houver algum objeto estranho, tente remover com cuidado 👀🔧."
            "Depois disso, o ATM ficou operante?",
            "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂",
        ], 
                           
        "flow2": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Vamos tentar resolver o seu problema no teclado"
            "Passo 1: Verifique se há no teclado do cliente algum sinal de vandalismo, como objeto (ex: clipe, papel, etc.) travando alguma tecla.\n"
            "Se houver algum objeto estranho, tente remover com cuidado 👀🔧."
            "Depois disso, o ATM ficou operante?",
            "Passo 2: desligue totalmente o equipamento. Após 10 segundos, ligue novamente."
            "Conseguimos resolver o seu problema?",
            "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️."
            "Caso não saiba transferir assista o vídeo abaixo 👇"
        ],
        "flow3": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Vamos tentar resolver o seu problema no teclado"
            "Passo 1: Verifique se há no teclado do cliente algum sinal de vandalismo, como objeto (ex: clipe, papel, etc.) travando alguma tecla.\n"
            "Se houver algum objeto estranho, tente remover com cuidado 👀🔧."
            "Depois disso, o ATM ficou operante?",
            "Passo 2: desligue totalmente o equipamento. Após 10 segundos, ligue novamente."
            "Conseguimos resolver o seu problema?",
            "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂",

            ],
        
        },
    
    
    "Leitora de cartão": {"flow1": [
                         "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
                         "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
                        "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
                        
                        "Vamos tentar resolver seu problema na leitora de cartões 💳🔧. Passo 1: Vá para frente do equipamento. Verifique se há na leitora de cartão algum sinal de vandalismo (ex.: clipe, papel, etc.) impedindo o seu correto funcionamento. Se houver obstrução, tente removê-la.\n"
                        "O ATM está operante?",
                        "Passo 2: Desligue totalmente o equipamento, por meio do cartão de operador. Após 10 segundos, ligue novamente. OBS:⚠️ nunca desligue a máquina diretamente no botão físico."
                        "Conseguimos resolver o seu problema?",
                        "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️."
                        "Caso não saiba transferir assista o vídeo abaixo 👇"
                        "Conversa encerrada. Até a próxima! 👋🙂",
                        

                        
                        

                        
            
        ], 
                        "flow2": [
                        "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador."
                         "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
                        "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
                        "Vamos tentar resolver seu problema na leitora de cartões 💳🔧. Passo 1: Vá para frente do equipamento. Verifique se há na leitora de cartão algum sinal de vandalismo (ex.: clipe, papel, etc.) impedindo o seu correto funcionamento. Se houver obstrução, tente removê-la"
                        "O ATM está operante?"
                        "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Se precisar de mais ajuda no futuro, estarei aqui para te auxiliar. Até a próxima! 👋🌟"
                         
                        ],
                        "flow3":[
                            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
                         "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
                        "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
                        "Vamos tentar resolver seu problema na leitora de cartões 💳🔧. Passo 1: Vá para frente do equipamento. Verifique se há na leitora de cartão algum sinal de vandalismo (ex.: clipe, papel, etc.) impedindo o seu correto funcionamento. Se houver obstrução, tente removê-la.\n"
                        "O ATM está operante?",
                        "Passo 2: Desligue totalmente o equipamento, por meio do cartão de operador. Após 10 segundos, ligue novamente. OBS:⚠️ nunca desligue a máquina diretamente no botão físico."
                        "Conseguimos resolver o seu problema?",
                        "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Se precisar de mais ajuda no futuro, estarei aqui para te auxiliar. Até a próxima! 👋🌟"
                        ]
                        
                        },
    
    
    "Security Way": 
        {"flow1": [
             "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
              "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
              "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
              "Identifiquei que o seu problema é relacionado ao security way"
              "Passo 1: Faça uma tentativa de pressionar alguma tecla do IOP (Painel do Operador). Ele deverá ativar após 5 segundos, sendo possível efetuar operações.\n"
              "IOP está ativo?"
              "Efetue uma tentativa de abertura offline. Após isso o equipamento abriu?"
              "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Se precisar de mais ajuda no futuro, estarei aqui para te auxiliar. Até a próxima! 👋🌟\\fim{#1}.\n"

            ], 
         "flow2": [
             "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
              "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
              "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
              "Identifiquei que o seu problema é relacionado ao security way"
              "Passo 1: Faça uma tentativa de pressionar alguma tecla do IOP (Painel do Operador). Ele deverá ativar após 5 segundos, sendo possível efetuar operações.\n"
              "IOP está ativo?"
              "O equipamento está operacional, ou seja, está sacando?",
              "Solicite ajuda técnica, use o código SW1."
              "Caso não saiba como realizar a abertura, veja o vídeo abaixo 👇"
              "Conversa encerrada. Até a próxima! 👋🙂"
             ],
         "flow3":
             [
              "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
              "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
              "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
              "Identifiquei que o seu problema é relacionado ao security way"
              "Passo 1: Faça uma tentativa de pressionar alguma tecla do IOP (Painel do Operador). Ele deverá ativar após 5 segundos, sendo possível efetuar operações.\n"
              "IOP está ativo?",
              "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Se precisar de mais ajuda no futuro, estarei aqui para te auxiliar. Até a próxima! 👋🌟"
             ],
             
            "flow4": [
              "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
              "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
              "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
              "Identifiquei que o seu problema é relacionado ao security way"
              "Passo 1: Faça uma tentativa de pressionar alguma tecla do IOP (Painel do Operador). Ele deverá ativar após 5 segundos, sendo possível efetuar operações.\n"
              "IOP está ativo?"
              "O equipamento está operacional, ou seja, está sacando?",
              "IA (SE Não): Solicite ajuda técnica, use o código SW2."
              "Caso não saiba como realizar a abertura, veja o vídeo abaixo 👇"
              "Conversa encerrada. Até a próxima! 👋🙂"
    
              ]
         
         },
    
    
    "Fechadura do cofre": 
        
        {"flow1": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Identifiquei que o seu problema é relacionado ao cofre do ATM 🔒"
            "Passo 1: Faça uma tentativa de abertura offline, acesse no IU Conecta a PR-425-16 ABERTURA DO CAIXA ELETRONICO COM MÓDULO OPERADOR OFF-LINE (PROBLEMA NO SECURITY WAY (BRASIL) da porta do cofre.\n"
            "Isso resolveu o seu problema?",
            "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂"
            ],
         
        "flow2": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Identifiquei que o seu problema é relacionado ao cofre do ATM 🔒"
            "Passo 1: Faça uma tentativa de abertura offline, acesse no IU Conecta a PR-425-16 ABERTURA DO CAIXA ELETRONICO COM MÓDULO OPERADOR OFF-LINE (PROBLEMA NO SECURITY WAY (BRASIL) da porta do cofre.\n"
            "Isso resolveu o seu problema?",
            "Quantos bips soaram ao digitar a senha de abertura do cofre?"
            "A abertura não foi liberada pelo Security Way(Erro Bloqueio). Efetue a liberação da fechadura através do Security Way(IOP)\n"
            "Conseguiu abrir o cofre?"
            "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Agora, é fundamental que estimule uma transação de saque no Caixa eletrônico para validar a solução do problema. Caso não seja possível, finalize a ocorrência manualmente dentro do Cockpit. Conversa encerrada. Até a próxima! 👋🙂"
            
            ],
        "flow3": [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Identifiquei que o seu problema é relacionado ao cofre do ATM 🔒"
            "Passo 1: Faça uma tentativa de abertura offline, acesse no IU Conecta a PR-425-16 ABERTURA DO CAIXA ELETRONICO COM MÓDULO OPERADOR OFF-LINE (PROBLEMA NO SECURITY WAY (BRASIL) da porta do cofre.\n"
            "Isso resolveu o seu problema?",
            "Quantos bips soaram ao digitar a senha de abertura do cofre?",
            "Sua senha foi aceita e a tranca por algum motivo está sem carga para abrir o cofre."
            "Retire o equipamento da tomada, abra um chamado técnico SW2 e aguarde contato da ilha técnica 👷‍♂️."
            "Caso não saiba como realizar a abertura, veja o vídeo abaixo 👇"
            "Conversa encerrada. Até a próxima! 👋🙂."
            
            ],
        "flow4":
            [
            "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Identifiquei que o seu problema é relacionado ao cofre do ATM 🔒"
            "Passo 1: Faça uma tentativa de abertura offline, acesse no IU Conecta a PR-425-16 ABERTURA DO CAIXA ELETRONICO COM MÓDULO OPERADOR OFF-LINE (PROBLEMA NO SECURITY WAY (BRASIL) da porta do cofre.\n"
            "Isso resolveu o seu problema?",
            "Quantos bips soaram ao digitar a senha de abertura do cofre?",
            "Sua senha não foi aceita. Tem certeza de que digitou a senha corretamente?",
            "Retire o equipamento da tomada, abra um chamado técnico SW2 e aguarde contato da ilha técnica 👷‍♂️."
            "Caso não saiba como realizar a abertura, veja o vídeo abaixo 👇"
            "Conversa encerrada. Até a próxima! 👋🙂"

             ],
        "flow5": [
             "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
            "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
            "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
            "Identifiquei que o seu problema é relacionado ao cofre do ATM 🔒"
            "Passo 1: Faça uma tentativa de abertura offline, acesse no IU Conecta a PR-425-16 ABERTURA DO CAIXA ELETRONICO COM MÓDULO OPERADOR OFF-LINE (PROBLEMA NO SECURITY WAY (BRASIL) da porta do cofre.\n"
            "Isso resolveu o seu problema?",
            "Quantos bips soaram ao digitar a senha de abertura do cofre?",
            "Sua senha não foi aceita. Tem certeza de que digitou a senha corretamente?",
            "Digite a senha corretamente e tente novamente, caso não dê certo abra um chamado técnico SW2"  ## TODO: Add another interaction
            "Caso não saiba como realizar a abertura, veja o vídeo abaixo 👇"
            "Conversa encerrada. Até a próxima! 👋🙂"
        ]
        },
    
    "Biometria": {
        "flow1":[
        "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
        "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
        "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧"
        "Identifiquei que o seu problema é relacionado à biometria"
        "Passo 1: Vá para frente do equipamento. Verifique se há, no leitor biométrico, algum sinal de vandalismo (ex.: clipe, papel, etc.) impedindo o seu correto funcionamento. Se houver obstrução, tente removê-la"
        "O ATM está operante?",
        "Passo 2: Desligue totalmente o equipamento, através do botão liga/desliga. Após 10 segundos, ligue novamente."
        "Conseguimos resolver o seu problema?",
        "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Se precisar de mais ajuda no futuro, estarei aqui para te auxiliar. Até a próxima! 👋🌟"
        ],
        
        "flow2": [
        "Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
        "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
        "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧",
        "Identifiquei que o seu problema é relacionado à biometria"
        "Passo 1: Vá para frente do equipamento. Verifique se há, no leitor biométrico, algum sinal de vandalismo (ex.: clipe, papel, etc.) impedindo o seu correto funcionamento. Se houver obstrução, tente removê-la"
        "O ATM está operante?",
        "Passo 2: Desligue totalmente o equipamento, através do botão liga/desliga. Após 10 segundos, ligue novamente."
        "Conseguimos resolver o seu problema?",
        "Neste caso não conseguiremos resolver o seu problema transfira o chamado com o código 016 👷‍♂️."
        "Caso não saiba transferir assista o vídeo abaixo 👇 "
        "Conversa encerrada. Até a próxima! 👋🙂"

        ], "flow3": ["Olá Ituber! Eu sou a Dani 🤖, sua assistente virtual da Diebold. Estou aqui para ajudar com os módulos: impressora de recibos, teclado, leitora de cartão, display do ATM, porta do cofre, scanner de cheques, security way, biometria e módulo reciclador. \n"
        "Antes de prosseguir, verifique se o seu problema é sistêmico e ocorre com outros equipamentos da agência ou se há incidente aberto no COR Informa.\n"
        "De preferência informe o código da ocorrência técnica no painel do CEI, caso não exista descreva o problema que o ATM está apresentando 😊🔧"
        "Identifiquei que o seu problema é relacionado à biometria"
        "Passo 1: Vá para frente do equipamento. Verifique se há, no leitor biométrico, algum sinal de vandalismo (ex.: clipe, papel, etc.) impedindo o seu correto funcionamento. Se houver obstrução, tente removê-la"
        "O ATM está operante?",
        "Fico feliz em saber que conseguimos resolver o seu problema! 😊 Se precisar de mais ajuda no futuro, estarei aqui para te auxiliar. Até a próxima! 👋🌟"
        ]
        
        },
}
