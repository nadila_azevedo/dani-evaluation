flows = {
    "Reciclador": {
        "flow1": [
            "olá",
            "Módulo reciclador",
            "sim",
            "sim",
            "consegui",
            "sim, consegui",
            "eu consegui fazer isso",
            "sim",
    
        ],
        "flow2": [
            "oi", 
            "Reciclador",
            "não consegui",
        ],
        "flow3": [
            "oi", 
            "Reciclador", 
            "sim", 
            "não consegui",
        ],
        "flow4": [
            "oi",
            "reciclador",
            "sim",
            "consegui",
            "não consegui",
        ],
        "flow5": [
            "oi",
            "reciclador",
            "sim",
            "consegui",
            "sim, consegui",
            "não consegui",
        ],
        "flow6": [
            "oi",
            "reciclador",
            "sim",
            "consegui",
            "sim, consegui",
            "eu consegui fazer isso",
            "não consegui",
        ],
        "flow7": [
            "oi",
            "reciclador",
            "sim",
            "consegui",
            "sim, consegui",
            "eu consegui fazer isso",
            "sim, eu consegui",
            "não consegui",
        ]
        
    },
    "Impressora de recibos": {
        "flow1": [
            "olá",
            "Impressora de recibos",
            "sim",
            "sim",
            "sim",
            "sim"
        ],
        "flow2": [
            "olá",
            "Impressora de recibos",
            "não consegui",
        ],
        "flow3": [
            "olá",
            "Impressora de recibos",
            "sim",
            "não consegui",
        ],
        "flow4": [
            "olá",
            "Impressora de recibos",
            "sim",
            "sim",
            "não consegui",
        ],
        "flow5": [
            "olá",
            "Impressora de recibos",
            "sim",
            "sim",
            "sim",
            "não consegui",
        ],
    },
    "Teclado do cliente": {
        "flow1": [
            "olá",
            "Teclado do cliente",
            "sim"
          
        ],
        "flow2": [
            "oi",
            "Teclado do cliente",
            "sim",
            "Não"   
        ],
        "flow3":[
            "oi",
            "Teclado do cliente",
            "sim",
            "Sim"   
        ]
    },
    "Monitor de vídeo": {
        "flow1": [
            "olá", 
            "Monitor de vídeo", 
            "sim", 
        ],
        "flow2": [
            "oi",
            "Monitor de vídeo",
            "não",
        ],
        "flow3": [
            "oi",
            "Monitor de vídeo",
            "sim",
            "não",
        ],
        "flow4": [
            "oi",
            "Monitor de vídeo",
            "sim",
            "sim",
            "não",
        ],
        "flow5": [
            "oi",
            "Monitor de vídeo",
            "sim",
            "sim",
            "sim",
            "não",
        ],
    },
    "Scanner de cheques": {
        "flow1": [
            "olá",
            "Scanner de cheques",
            "sim",
            "sim",
            "sim",
        ],
        "flow2": [
            "oi",
            "Scanner de cheques",
            "não",
        ],
        "flow3": [
            "oi",
            "Scanner de cheques",
            "sim",
            "não",
        ],
        "flow4": [
            "oi",
            "Scanner de cheques",
            "sim",
            "certamente",
            "não"
        ],
    },
    
    "Leitora de cartão": {
        "flow1": ["olá", "Leitora de cartão", "sim", "sim", "sim", "sim", "sim", "sim"],
        "flow2": [
            "oi",
            "Leitora de cartão",
            "sim",
            "certamente",
            "claro",
            "ok",
            "entendi",
            "sim, por favor",
        ],
    },
    "Security Way": {
        "flow1": ["olá", "Security Way", "Não", "Sim"],
        "flow2": [
            "olá",
            "Security Way",
            "Não",
            "Não"
        ],
        "flow3":[
            "olá",
            "Security Way",
            "Sim",
        ]
    },
    "Fechadura do cofre": {
        "flow1": [
            "olá",
            "Fechadura do cofre",
            "Atm está operante"
           
        ],
        "flow2": [
            "oi",
            "Fechadura do cofre",
            "Não operante",
            "7 bips",
            "sim"
        ],
        "flow3":[
            "oi",
            "Fechadura do cofre",
            "Não operante",
            "2 bips"
            
        ],
        "flow4":[
            "oi",
            "Fechadura do cofre",
            "Não operante",
            "3 bips",
            "Sim"
        ],
        "flow5":[
            "oi",
            "Fechadura do cofre",
            "Não operante",
            "3 bips",
            "Não"
        ]
        ,
    },
    "Biometria": {
        "flow1": ["olá", "Biometria não está funcionando", "sim", "atm operante"],
        "flow2": [
            "olá",
            "Biometria não está funcionando",
            "Não operante",
            "Não operante"
        ],
        "flow3":[
            "olá",
            "Biometria não está funcionando",
            "atm operante",
        ]
    },
}
